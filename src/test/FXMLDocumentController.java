/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button button;

    private Connection conn;
    private PreparedStatement stmt = null;
    @FXML
    private Label label1;
    @FXML
    private Label label2;
    @FXML
    private Button button1;
    @FXML
    private Button button2;
    @FXML
    private Label label11;
    @FXML
    private Button button11;

    @FXML
    private void handleButtonAction(ActionEvent event) {
        doconnect();
    }
    @FXML
    private void handleButtonAction1(ActionEvent event) {
        doconnect();
        doInsert();
    }

    @FXML
    private void handleButtonAction2(ActionEvent event) {
        doconnect();
        doselect1();
    }
    @FXML
    private void handleButtonAction3(ActionEvent event) {
        doconnect();
        doselect2();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    private int doInsert() {
        if (conn == null) {
            doconnect();
        }
        String[] f1 = {"小明","小華","小美","小黑","小白"};
        String[] f2 = {"被狗咬","走在路上沒事","撿到1000塊","中了頭獎100萬","是成年肥宅"};
        Random r = new Random();
        int rows = 0;
        String insert = "insert into TEST.COLLEAGUES values (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement stmt = null;
        try {

            stmt = conn.prepareStatement(insert);
            stmt.setInt(1, (int)(Math.random()*100+1));
            stmt.setString(2, f1[r.nextInt(5)]);
            stmt.setString(3, f2[r.nextInt(5)]);
            stmt.setString(4, "JhonCena");
            stmt.setString(5, "SuperSlamp~");
            stmt.setString(6, "timmy60522@gmail.com");
            stmt.setInt(7, 0);
            rows = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return rows;
        }
    }

    private int doselect1() {
        if (conn == null) {
            doconnect();
        }

        int rows = 0;
        String select = "select * from TEST.COLLEAGUES";
        try {
            stmt = conn.prepareStatement(select);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
               label2.setText(result.getString(1));
            }
            stmt.close();
            result.close();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return rows;
        }
    }
    private int doselect2() {
        if (conn == null) {
            doconnect();
        }

        int rows = 0;
        String select = "select * from TEST.COLLEAGUES";
        try {
            stmt = conn.prepareStatement(select);
            ResultSet result = stmt.executeQuery();
            while (result.previous()) {
               label2.setText(result.getString(1));
            }
            stmt.close();
            result.close();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                stmt.close();
                conn.close();
            } catch (SQLException ex) {
                Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return rows;
        }
    }

    private void doconnect() {
        try {
            conn = DriverManager.getConnection("jdbc:derby://localhost:1527/contact", "test", "test");
            label1.setText("連線成功");
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            label1.setText("連線失敗");
        }
    }

    

}
